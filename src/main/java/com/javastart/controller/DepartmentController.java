package com.javastart.controller;

import com.javastart.exception.EmployeeNotFoundException;
import com.javastart.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/department")
public class DepartmentController {

    private final DepartmentService departmentService;

    @Autowired
    public DepartmentController(DepartmentService departmentService) {
        this.departmentService = departmentService;
    }

    @GetMapping("/{id}/countHours")
    public Integer getDepartmentHoursWorked(@PathVariable Long id) {
       try {
           return departmentService.getDepartmentHoursWorked(id);
       }catch (EmployeeNotFoundException e){
           return -1;
       }
    }

    @GetMapping("/{id}/countEmployeesAtWork")
    public Integer getCountEmployeesAtWork(@PathVariable Long id) {
        try {
            return departmentService.getCountEmployeesAtWork(id);
        }catch (EmployeeNotFoundException e){
            return 0;
        }
    }

    @GetMapping("{id}/countEmployeesWalked")
    public Integer getCountEmployeesDepartmentWalked(@PathVariable Long id) {
        try {
            return departmentService.getCountEmployeesDepartmentWalked(id);
        }catch (EmployeeNotFoundException e){
            return 0;
        }
    }
}

package com.javastart.controller.dto;

import com.javastart.entity.Employee;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.Date;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class EmployeeResponseDTO {

    private String name;

    private String surname;

    private Long workedHours;

    private Date birthday;

    public EmployeeResponseDTO(Employee employee) {
        this.name = employee.getName();
        this.surname = employee.getSurname();
        this.birthday = employee.getBirthday();
        this.workedHours = employee.getWorkedHours();

    }

}


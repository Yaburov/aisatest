package com.javastart.controller.dto;

import com.javastart.entity.Vacation;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class VacationResponseDTO {

    private LocalDate startOfVacation;

    private LocalDate endOfVacation;

    public VacationResponseDTO(Vacation vacation) {
        this.startOfVacation = vacation.getStartOfVacation();
        this.endOfVacation = vacation.getEndOfVacation();
    }
}

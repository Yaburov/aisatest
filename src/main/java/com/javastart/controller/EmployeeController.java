package com.javastart.controller;

import com.javastart.controller.dto.EmployeeResponseDTO;
import com.javastart.exception.EmployeeNotFoundException;
import com.javastart.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.List;

@Controller
@RequestMapping("/employee")
public class EmployeeController {


    private final EmployeeService employeeService;

    @Autowired
    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @GetMapping("/{id}/countWorkDays")
    public Integer getCountEmployeeDaysWorked(@PathVariable Long id) {
        try {
            return employeeService.getInfoAbouWorkedDays(id);
        }catch (EmployeeNotFoundException e){
            return -1;
        }
    }

    @GetMapping("/{id}/countWalkedDays")
    public Integer getCountEmployeeDaysWalked(@PathVariable Long id){
        return employeeService.countInfoAbouWalkedDays(id);
    }

    @GetMapping("/{id}/countWorkHours")
    public EmployeeResponseDTO getEmployeeHoursWorked(@PathVariable Long id) throws EmployeeNotFoundException {
            return new EmployeeResponseDTO(employeeService.getInfoAbouWorkedHours(id));
    }

    @GetMapping("/{id}/checkEmployeeOnWork")
    public String checkEmployeeOnWork(@PathVariable Long id) {
        return employeeService.checkEmployeeOnWork(id);
    }

    @GetMapping("/{id}/checkEmployeeNotOnWork")
    public String checkEmployeeNotOnWork(@PathVariable Long id) {
        return employeeService.checkEmployeeNotOnWork(id);
    }

    @GetMapping("/{id}/birthday")
    public EmployeeResponseDTO getAllBirthdayEmployees(@PathVariable Long id) throws EmployeeNotFoundException {
        return new EmployeeResponseDTO(employeeService.getBirthday(id));
    }

    @GetMapping("/birthdays")
    public List<LocalDate> getBirthdayEmployee() {
        return employeeService.getAllBirthdaysInThisYear();
    }
}


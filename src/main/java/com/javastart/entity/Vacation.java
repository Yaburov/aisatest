package com.javastart.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table
public class Vacation {

    public Vacation(LocalDate startOfVacation, LocalDate endOfVacation, boolean activeVacation, Employee employee) {
        this.startOfVacation = startOfVacation;
        this.endOfVacation = endOfVacation;
        this.activeVacation = activeVacation;
        this.employee = employee;
    }

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private LocalDate startOfVacation;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getStartOfVacation() {
        return startOfVacation;
    }

    public void setStartOfVacation(LocalDate startOfVacation) {
        this.startOfVacation = startOfVacation;
    }

    public LocalDate getEndOfVacation() {
        return endOfVacation;
    }

    public void setEndOfVacation(LocalDate endOfVacation) {
        this.endOfVacation = endOfVacation;
    }

    public boolean isActiveVacation() {
        return activeVacation;
    }

    public void setActiveVacation(boolean activeVacation) {
        this.activeVacation = activeVacation;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Vacation() {
    }

    private LocalDate endOfVacation;

    private boolean activeVacation;

    @JsonBackReference
    @ManyToOne()
    private Employee employee;
}

package com.javastart.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table
public class Hospital {

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Hospital(LocalDate beginningSickLeave, LocalDate endSickLeave, boolean activeSickLeave, Employee employee) {
        this.beginningSickLeave = beginningSickLeave;
        this.endSickLeave = endSickLeave;
        this.activeSickLeave = activeSickLeave;
        this.employee = employee;
    }

    public LocalDate getBeginningSickLeave() {
        return beginningSickLeave;
    }

    public void setBeginningSickLeave(LocalDate beginningSickLeave) {
        this.beginningSickLeave = beginningSickLeave;
    }

    public Hospital() {
    }

    public LocalDate getEndSickLeave() {
        return endSickLeave;
    }

    public void setEndSickLeave(LocalDate endSickLeave) {
        this.endSickLeave = endSickLeave;
    }

    public boolean isActiveSickLeave() {
        return activeSickLeave;
    }

    public void setActiveSickLeave(boolean activeSickLeave) {
        this.activeSickLeave = activeSickLeave;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private LocalDate beginningSickLeave;

    private LocalDate endSickLeave;

    private boolean activeSickLeave;

    @JsonBackReference
    @ManyToOne()
    private Employee employee;
}

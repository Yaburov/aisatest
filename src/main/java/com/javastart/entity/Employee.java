package com.javastart.entity;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.List;


@Entity
@Table
public class Employee {

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;

    public Employee() {
    }

    public Long getId() {
        return id;
    }

    public Employee(String name, String surname, Integer age, List<Date> attendanceList, List<Date> absenteeismList, Date birthday, Long workedHours, Department department, List<Vacation> vacation, List<Hospital> hospitals) {
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.attendanceList = attendanceList;
        this.absenteeismList = absenteeismList;
        this.birthday = birthday;
        this.workedHours = workedHours;
        this.department = department;
        this.vacation = vacation;
        this.hospitals = hospitals;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public List<Date> getAttendanceList() {
        return attendanceList;
    }

    public void setAttendanceList(List<Date> attendanceList) {
        this.attendanceList = attendanceList;
    }

    public List<Date> getAbsenteeismList() {
        return absenteeismList;
    }

    public void setAbsenteeismList(List<Date> absenteeismList) {
        this.absenteeismList = absenteeismList;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public Long getWorkedHours() {
        return workedHours;
    }

    public void setWorkedHours(Long workedHours) {
        this.workedHours = workedHours;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public List<Vacation> getVacation() {
        return vacation;
    }

    public void setVacation(List<Vacation> vacation) {
        this.vacation = vacation;
    }

    public List<Hospital> getHospitals() {
        return hospitals;
    }

    public void setHospitals(List<Hospital> hospitals) {
        this.hospitals = hospitals;
    }

    private String surname;

    private Integer age;

    @ElementCollection
    private List<Date> attendanceList;

    @ElementCollection
    private List<Date> absenteeismList;

    @Temporal(TemporalType.DATE)
    private Date birthday;

    private Long workedHours;


    @ManyToOne
    @JoinColumn()
    private Department department;

    @OneToMany
    private List<Vacation> vacation;

    @OneToMany
    private List<Hospital> hospitals;
}
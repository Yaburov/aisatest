package com.javastart.repository;

import com.javastart.entity.Vacation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface VacationRepository extends JpaRepository<Vacation, Long> {

    @Query(nativeQuery = true, value = "SELECT * FROM vacation WHERE employee_id = :id")
    List<Vacation> findEmployeeVacation(@Param("id") Long id);
}

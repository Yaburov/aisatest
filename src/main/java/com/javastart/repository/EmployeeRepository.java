package com.javastart.repository;

import com.javastart.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {

    @Query(value = "SELECT u FROM  Employee u WHERE u.id =:id")
    Optional<Employee> findById(@Param("id") Long id);

    @Query(value = "SELECT u FROM  Employee u WHERE u.id =:id")
    Optional<Employee> findByIdWorkedHours(@Param("id") Long id);

    @Query(value = "SELECT u FROM Employee u WHERE u.birthday BETWEEN :curDate AND :endOfYear")
    List<Employee> findAllByBirthdayBetween(Date curDate, Date endOfYear);

    @Query(nativeQuery = true, value = "select count(employee_id) from employee_attendance_list where employee_id =:employee_id")
    Integer findEmployeeAttendanceListById(@Param("employee_id") Long id);

    @Query(nativeQuery = true, value = "SELECT COUNT(employee_id) FROM employee_absenteeism_list where employee_id =:employee_id")
    Integer findEmployeeByAbsenteeismList(@Param("employee_id") Long id);
}

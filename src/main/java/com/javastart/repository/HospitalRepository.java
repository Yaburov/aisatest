package com.javastart.repository;

import com.javastart.entity.Hospital;
import com.javastart.entity.Vacation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface HospitalRepository extends JpaRepository<Vacation, Long> {

    @Query(nativeQuery = true, value = "SELECT * FROM hospital WHERE employee_id = :id")
    List<Hospital> findEmployeeSick(@Param("id") Long id);
}

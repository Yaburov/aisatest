package com.javastart.repository;

import com.javastart.entity.Department;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

public interface DepartmentRepository extends JpaRepository<Department, Long> {

    @Query(value = "SELECT u FROM Department u WHERE u.id =:id")
    Optional<Department> findById(@Param("id") Long id);

    @Query(nativeQuery = true, value = "select\n" +
            "    case\n" +
            "        when count(e.id) != 0 then sum(e.worked_hours)\n" +
            "        when count(e.id) = 0 then 0\n" +
            "    end\n" +
            "from department d left join employee e on d.id = e.department_id\n" +
            "where d.id = :department_id\n" +
            "group by d.id")
    Integer findWorkedHoursDepartmentById(@Param("department_id") Long id);

    @Query(nativeQuery = true, value = "select\n" +
            "            case\n" +
            "                   when count(e.id) != 0 then count(e.id)\n" +
            "                   when count(d.id) = 0 then 0\n" +
            "            end\n" +
            "            from department d\n" +
            "                     join employee e on d.id = e.department_id\n" +
            "                     join employee_attendance_list eal on e.id = eal.employee_id\n" +
            "group by e.department_id\n" +
            "HAVING department_id =:department_id")
    Integer findCountEmployeesDepartmentAtWorkById(@Param("department_id") Long id);

    @Query(nativeQuery = true, value = "SELECT COUNT(department_id) FROM department\n" +
            "JOIN employee e on department.id = e.department_id\n" +
            "JOIN employee_absenteeism_list eal on e.id = eal.employee_id\n" +
            "GROUP BY e.department_id\n" +
            "HAVING department_id=:department_id")
    Integer findCountEmployeesDepartmentWalked(@Param("department_id") Long id);
}

package com.javastart.exception;

public class EmployeeDayOffException extends RuntimeException{

    public EmployeeDayOffException(String s) {
        super(s);
    }
}

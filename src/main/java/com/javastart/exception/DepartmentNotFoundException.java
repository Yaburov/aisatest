package com.javastart.exception;

public class DepartmentNotFoundException extends Exception{

    public DepartmentNotFoundException(String s) {
        super(s);
    }
}

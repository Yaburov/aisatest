package com.javastart.exception;

public class SickNotFoundException extends RuntimeException {

    public SickNotFoundException(String message) {
        super(message);
    }
}

package com.javastart.exception;

public class EmployeeNotFoundException extends Exception{

    public EmployeeNotFoundException(String s) {
        super(s);
    }
}

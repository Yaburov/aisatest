package com.javastart.exception;

public class VacationNotFoundException extends RuntimeException {

    public VacationNotFoundException(String message) {
        super(message);
    }
}

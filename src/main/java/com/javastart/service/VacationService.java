package com.javastart.service;

import com.javastart.entity.Vacation;
import com.javastart.exception.VacationNotFoundException;
import com.javastart.repository.VacationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class VacationService {

    private final VacationRepository vacationRepository;

    @Autowired
    public VacationService(VacationRepository vacationRepository) {
        this.vacationRepository = vacationRepository;
    }

    public Vacation getVocationInfo(Long id) {
        List<Vacation> vacations = vacationRepository.findEmployeeVacation(id);
        if (vacations == null || vacations.isEmpty()) {
            throw new VacationNotFoundException("Can't find employee vacation with id: " + id);
        }
        Vacation lastVacation = vacations.get(0);
        LocalDate tempVacationDate = lastVacation.getStartOfVacation();
        for (Vacation vacation : vacations) {
            LocalDate currentDateOfVacation = vacation.getStartOfVacation();
            if (currentDateOfVacation.compareTo(tempVacationDate) > 0) {
                tempVacationDate = currentDateOfVacation;
                lastVacation = vacation;
            }
        }
        return lastVacation;
    }
}

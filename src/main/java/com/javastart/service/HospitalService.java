package com.javastart.service;

import com.javastart.entity.Hospital;
import com.javastart.exception.SickNotFoundException;
import com.javastart.repository.HospitalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class HospitalService {

    private final HospitalRepository hospitalRepository;

    @Autowired
    public HospitalService(HospitalRepository hospitalRepository) {
        this.hospitalRepository = hospitalRepository;
    }

    public Hospital getHospitalInfo(Long id) {
        List<Hospital> hospitals = hospitalRepository.findEmployeeSick(id);
        if (hospitals == null || hospitals.isEmpty()) {
            throw new SickNotFoundException("Can't find sick with id: " + id);
        }
        Hospital lastHospital = hospitals.get(0);
        LocalDate tempVacationDate = lastHospital.getBeginningSickLeave();
        for (Hospital hospital : hospitals) {
            LocalDate currentDateOfHospital = hospital.getBeginningSickLeave();
            if (currentDateOfHospital.compareTo(tempVacationDate) > 0) {
                tempVacationDate = currentDateOfHospital;
                lastHospital = hospital;
            }
        }
        return lastHospital;
    }
}

package com.javastart.service;

import com.javastart.exception.EmployeeNotFoundException;
import com.javastart.repository.DepartmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class DepartmentService {

    private final DepartmentRepository departmentRepository;

    @Autowired
    public DepartmentService(DepartmentRepository departmentRepository) {
        this.departmentRepository = departmentRepository;
    }

    public Integer getDepartmentHoursWorked(Long id) throws EmployeeNotFoundException {
        return Optional.ofNullable(departmentRepository.findWorkedHoursDepartmentById(id))
                .orElseThrow(()->new EmployeeNotFoundException(""));
    }

    public Integer getCountEmployeesAtWork(Long id) throws EmployeeNotFoundException {
        return Optional.ofNullable(departmentRepository.findCountEmployeesDepartmentAtWorkById(id))
                .orElseThrow(()->new EmployeeNotFoundException(""));
    }

    public Integer getCountEmployeesDepartmentWalked(Long id) throws EmployeeNotFoundException {
        return Optional.ofNullable(departmentRepository.findCountEmployeesDepartmentWalked(id))
                .orElseThrow(()->new EmployeeNotFoundException(""));
    }
}

package com.javastart.service;

import com.javastart.entity.Employee;
import com.javastart.exception.EmployeeDayOffException;
import com.javastart.exception.EmployeeNotFoundException;
import com.javastart.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class
EmployeeService {

    private final EmployeeRepository employeeRepository;

    @Autowired
    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }


    public Employee getInfoAbouWorkedHours(Long id) throws EmployeeNotFoundException {
        return employeeRepository.findByIdWorkedHours(id)
                .orElseThrow(() -> new EmployeeNotFoundException("Can't find employee with id: " + id));
    }

    public Integer getInfoAbouWorkedDays(Long id) throws EmployeeNotFoundException {
        return Optional.of(employeeRepository.findEmployeeAttendanceListById(id))
                .orElseThrow(()-> new EmployeeNotFoundException(""));
    }

    public Integer countInfoAbouWalkedDays(Long id) {
        return employeeRepository.findEmployeeByAbsenteeismList(id);
    }

    public Employee getBirthday(Long id) throws EmployeeNotFoundException {
        return employeeRepository.findById(id)
                .orElseThrow(() -> new EmployeeNotFoundException("Can't find employee with id: " + id));
    }

    public List<LocalDate> getAllBirthdaysInThisYear() {
        LocalDate now = LocalDate.now();
        LocalDate yearEnd = LocalDate.ofYearDay(now.getYear() + 1, 1);
        Date nowSQL = java.sql.Date.valueOf(now);
        Date endYearSQL = java.sql.Date.valueOf(yearEnd);

        List<Employee> employees = employeeRepository.findAllByBirthdayBetween(nowSQL, endYearSQL);
        List<LocalDate> birthdays = employees.stream().map(e -> new java.sql.Date(e.getBirthday().getTime()).toLocalDate()).collect(Collectors.toList());
        return birthdays;
    }

    public String checkEmployeeOnWork(Long id) throws EmployeeDayOffException {
        LocalDate now = LocalDate.now();
        if (now.getDayOfWeek() != DayOfWeek.SATURDAY && now.getDayOfWeek() != DayOfWeek.SUNDAY) {
            Employee employee = employeeRepository.findById(id).get();
            employee.getAttendanceList().add(java.sql.Date.valueOf(now));
            employeeRepository.save(employee);
            return "The operation was successful";
        } else {
            return new EmployeeDayOffException("Today is a day off, the office is closed").getMessage();
        }
    }

    public String checkEmployeeNotOnWork(Long id) throws EmployeeDayOffException {
        LocalDate now = LocalDate.now();
        if (now.getDayOfWeek() != DayOfWeek.SATURDAY && now.getDayOfWeek() != DayOfWeek.SUNDAY) {
            Employee employee = employeeRepository.findById(id).get();
            employee.getAbsenteeismList().add(java.sql.Date.valueOf(now));
            employeeRepository.save(employee);
            return "The operation was successful";
        } else {
            return new EmployeeDayOffException("Today is a day off, the office is closed").getMessage();
        }
    }
}
